import numpy as np
from PIL import Image
def fileToNumArray(filename):
    """Takes a file and returns an array of ints"""
    with open(filename,'r') as file:
        string = file.read()
    list = []
    for i in range(0,len(string)):
        list.append(ord(string[i]))
    return np.array(list,dtype=np.uint8)

def imageFileToArray(filename):
    """Takes an image file and returns it as an array"""
    image = Image.open(filename)
    return np.array(image,dtype=np.uint8)

def dataFitsInImage(data_array,image_array):
    """Takes an array of nums and an image array and returns len(data)<len(image[dimensions]"""
    data_length = len(data_array)
    image_length = len(image_array)*len(image_array[0])
    return data_length < image_length

def saveOriginal(filename):
    """Takes a file and saves it with a original- prefix"""
    image = Image.open(filename)
    image.save('original-'+filename)

def dataInImArray(data,image_array):
    """Takes an array of data and saves it in a part of an image array returns a new image array"""
    j = 0
    k = 0
    new_array = np.array(image_array,dtype=np.uint8)
    for i in range(0,len(data)):
        new_array[j][k][3] = image_array[j][k][3]-data[i]
        if(j+1<len(image_array)):
            j+=1
        else:
            j=0
            k+=1
    return new_array

def saveImage(image_array, new_file_name):
    """Takes an image array and saves it as a png file with the new_file_name"""
    image = Image.fromarray(image_array)
    image.save(new_file_name)


def dataToImage(image_filename,data_filename):
    """Takes data as a (text) file and saves it in the alpha of an image. It saves the original image (needed for decoding) and the new datafilled image."""
    data = fileToNumArray(data_filename)
    image_array = imageFileToArray(image_filename)
    if dataFitsInImage(data,image_array):
        saveOriginal(image_filename)
        data_filled_image_array = dataInImArray(data,image_array)
        saveImage(data_filled_image_array,image_filename)
    else:
        print("Image is too small for this set of data")
def numarraysToNumArray(key_array, cypher_array):
    """Takes a key array and a cypher array and returns a decyphered array"""
    decyphered_array = []
    for i in range(0,len(key_array)):
        for j in range(0,len(key_array[0])):
            if(key_array[i][j][3] != cypher_array[i][j][3]):
                decyphered_array.append(key_array[i][j][3]-cypher_array[i][j][3])
    return np.array(decyphered_array,dtype=np.uint8)

def numArrayToString(num_array):
    """Takes a num array and returns a string"""
    string = ""
    for i in range(0,len(num_array)):
        string = string + chr(num_array[i])
    return string
        
def stringDataToFile(string,filename):
    """Takes a string and saves it in a file"""
    with open(filename, 'w') as file:
        file.write(string)

def decodeImageData(key_image,data_image,data_save_target):
    """Takes an image as a key and an image filled with hidden data and saves the data in a text file."""
    key = imageFileToArray(key_image)
    cypher = imageFileToArray(data_image)
    data_num_array = numarraysToNumArray(key,cypher)
    data_string = numArrayToString(data_num_array)
    data_file = stringDataToFile(data_string,data_save_target)

